// SDL2 frontend for the game

#include <stdio.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_image.h>

#undef main

#include "game.h" // keep in mind, MAIN can still access variables from the game!

Mix_Music *music[3];

Mix_Chunk *sfx[12];

//SDL_Surface *winsurf;
SDL_Texture *wintext;

//SDL_Texture *sprites[20];

SDL_Rect dspdest_rect; // this is the DESTINATION, aka screen position

SDL_Point zeropoint = {0,0};

SDL_Renderer *render;
SDL_Window *win;

void drawSprite(SDL_Texture *sprite, int x, int y) {
	dspdest_rect = (SDL_Rect){x, y, 0, 0};
	SDL_QueryTexture(sprite, NULL, NULL, &dspdest_rect.w, &dspdest_rect.h);
	SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
};

void drawRepeatingSprite(SDL_Texture *sprite, int x, int y) { // this DOESN'T repeat yet, because I am big Stupid(TM)
	int _x = x;
	if (x == y) {
		dspdest_rect.x = dspdest_rect.y = x;
	} else {
		dspdest_rect.x = x;
		dspdest_rect.y = y;
	}
	SDL_QueryTexture(sprite, NULL, NULL, &dspdest_rect.w, &dspdest_rect.h);
	uint8_t limit = 0;
	for (; _x < 64 && limit <= 30; limit++) {
		if (_x < (-64 << 2)) {
			_x += dspdest_rect.w;
			continue;
		};
		SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
		_x += dspdest_rect.w;
		dspdest_rect.x = _x;
	};
	limit = 0;
	_x = x;
	for (; _x > -dspdest_rect.w && limit <= 30; limit++) {
		SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
		_x -= dspdest_rect.w;
		dspdest_rect.x = _x;
	};
};

void drawSpriteSheeted(SDL_Texture *sprite, int x, int y, int frame, int x_sprdist, int y_sprdist, uint8_t flipped) { // supports only purely horizontal sheets
	if (x < -x_sprdist || x > x_sprdist + 64) return;
	SDL_Rect destrect = {x, y, x_sprdist, y_sprdist};
	dspdest_rect.x = x_sprdist * frame;
	dspdest_rect.y = 0; // this somehow fixes an issue where the sprite would vertically stretch when moving the camera vertically, but crop off within the sprite's intended bounds. what?
	dspdest_rect.w = x_sprdist,
	dspdest_rect.h = y_sprdist,
	SDL_RenderCopyEx(render, sprite, &dspdest_rect, &destrect, 0, &zeropoint, flipped ? SDL_FLIP_HORIZONTAL : SDL_FLIP_NONE);
};

void draw() {

};

uint8_t *keystates;

uint8_t keyPressed(uint8_t index) {};

int main() {
	if (SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_EVENTS |
 SDL_INIT_JOYSTICK) != 0) return 1;
	
	#define REALWINTITLE "Celestia"
	win = SDL_CreateWindow(REALWINTITLE, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 256, 256, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	render = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	#undef REALWINTITLE
	
	SDL_RenderSetLogicalSize(render, 64, 64); // keep that cwispy 270P 16:9 wesowutiown UwU
	
	SDL_SetRenderDrawBlendMode(render, SDL_BLENDMODE_NONE);
	
	//winsurf = SDL_GetWindowSurface(win);
	
	SDL_SetRenderDrawColor(render, 0, 0x01, 0x90, 255);
	keystates = SDL_GetKeyboardState(NULL); // this was called every time keys() is ran, but apparently it's only needed to be called ONCE :)
	
	uint8_t running = 1;
	SDL_Event event;
	start();
	while (running) {
		while (SDL_PollEvent(&event)) {
			switch (event.type)
			{
				case SDL_QUIT: running = 0; break;
			}
		}
		//keys();
        	draw();
		step();
		
		//wintext = SDL_CreateTextureFromSurface(render, winsurf);
		//SDL_RenderCopy(render, wintext, NULL, NULL);
		//SDL_UpdateWindowSurface(win);
		SDL_RenderPresent(render);
		//SDL_DestroyTexture(wintext);
		SDL_RenderClear(render);
		SDL_Delay(16.667f);
	}
	printf("Exiting game...\n");
	
	SDL_DestroyRenderer(render);
	SDL_DestroyWindow(win);
	SDL_Quit();
	
	return 0;
};
