#!/bin/sh
# Automatically runs the image conversion script to refresh all sprites.

python3 convert_image.py plr.png | sed "s/image/plrIMG/" > ../sprites.h
python3 convert_image.py enemy1.png | sed "s/image/enemy1IMG/" >> ../sprites.h
