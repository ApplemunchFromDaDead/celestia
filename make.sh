#!/bin/sh
# Compiler script for Celestia written by blitzdoughnuts
# with inspiration from drummyfish's own make.sh for Anarch and co.

if [ $# -eq 0  ]; then # no arguments?
	echo "Please enter an argument for the target frontend (sdl, saf, etc)"
	exit 1
fi

COMPILER="gcc"
OUTPUT="celestia"

if [ $1 == "saf" ]; then
	$COMPILER main_saf.c -lSDL2 -o $OUTPUT
fi
