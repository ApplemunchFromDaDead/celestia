// Configuration file for Celestia

// By default, Celestia uses minimal options, for both easing on your CPU and less clutter in-game

#ifndef CEL_PARTICLES
#define CEL_PARTICLES 0
#endif

// == GAMEPLAY OPTIONS ==

// How fast do enemies update in frames?
#ifndef CEL_ENEMY_TICRATE
#define CEL_ENEMY_TICRATE 6
#endif
