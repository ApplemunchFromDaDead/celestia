// Enemy swarm patterns for Celestia
// Licensed under CC0

#define SWARM_COUNT 16

// Individual swarms use this size as a constraint for keeping size low
#define SWARM_WIDTH 16
#define SWARM_HEIGHT 12

#define REALSWARMSIZE (SWARM_WIDTH * SWARM_HEIGHT) / 4

/*
  The pattern format should go as follows:
  Each byte will have 4 2-bit valies,
  which is enough for 3 enemy types. A
  basic thing, I know.

  aabbccdd
  a-d = enemies 1-4 on the map

  
 */

uint8_t swarm1[REALSWARMSIZE] = {
	0b00000000, 0b00000000, 0b00000000, 0b00000000,
	0b00000000, 0b11000110, 0b00000000, 0b00000000,
	0b00000000, 0b00000000, 0b00000000, 0b00000000,
};

void spawnSwarm(uint8_t *swarm) {
	for (uint8_t i = 0; i < REALSWARMSIZE; i++) printf("%i", swarm[i]); // DEBUG
	printf("\n"); // DEBUG
	for (uint8_t i = 0; i < REALSWARMSIZE; i++) {
		uint8_t ii = 0;
		#define SWARMCALC swarm[i] & (0b11000000 >> (ii * 2)) >> ((3 - ii) * 2)
		while (ii < 3) {
			switch (SWARMCALC)
			{
				case 0:
					// it'd be stupid to reset all values, just set the flag so the game knows to fill it in
					enemies[i + ii].flags = 0;
					break;
				case 1:
				case 2:
				case 3:
					enemies[i + ii].type = SWARMCALC;
					enemies[i + ii].x = (i + ii) & SWARM_WIDTH;
					enemies[i + ii].y = (i + ii) & SWARM_HEIGHT;
					enemies[i + ii].flags = 1;
					printf("%i %i %i || %i, %i, %i\n", i * 4, ii, i * 4 + ii, enemies[i + ii].type, enemies[i + ii].x, enemies[i + ii].y); /// DEBUG
					break;
			};
			ii++;
		};
	};
};
