// Game code for Celestia
// Licensed under CC0

#define GSTATE_MENU 0
#define GSTATE_PLAY 1

#define KEY_UP 1
#define KEY_DOWN 1 << 1
#define KEY_LEFT 1 << 2
#define KEY_RIGHT 1 << 3
#define KEY_FIRE 1 << 4

#include "config.h"

uint8_t GAMESTATE;

typedef struct {
	uint8_t x1, x2;
	uint8_t y1, y2;
} BBOX;

struct {
  uint8_t x;
  uint8_t y;
  uint8_t lives;
  BBOX bbox;
} plr; // alternatively, CEL_Player

typedef struct {
  uint8_t x;
  uint8_t y;
  uint8_t type:4;
  uint8_t flags:4;
  BBOX bbox;
} CEL_Enemy;

typedef struct {
  uint8_t x;
  uint8_t y;
  uint8_t flags;
} CEL_Bullet;

uint8_t enemyUpdateTimer;

CEL_Enemy enemies[64];
uint8_t enemyCount;
CEL_Bullet bullets[4];

#include "patterns.h"

struct {
  uint8_t selindex;
} menu;

uint8_t keyPressed(uint8_t index);

uint8_t pointCollidesWithBox(uint8_t x, uint8_t y, BBOX *bbox) {
	return x > bbox->x1 && x < bbox->x2 &&
	       y > bbox->y1 && y < bbox->y2;
};

void makeEnemy(uint8_t type, uint8_t x, uint8_t y) {
	for(uint8_t i = 0; i < 64; i++) {
		if (!(enemies[i].flags & 1)) {
			enemies[i].x = x;
			enemies[i].y = y;
			enemies[i].type = type;
			enemies[i].flags ^= 1;
			enemies[i].bbox.x1 = enemies[i].x - 4;
			enemies[i].bbox.x2 = enemies[i].x + 4;
			enemies[i].bbox.y1 = enemies[i].y - 4;
			enemies[i].bbox.y2 = enemies[i].y + 4;
			enemyCount++;
			break;
		};
	};
};

void start() {
	enemyCount = 0;
	enemyUpdateTimer = CEL_ENEMY_TICRATE;

	plr.x = 32;
	plr.y = 48;
	plr.lives = 3;

	//makeEnemy(0, 32, 4);
	//makeEnemy(0, 48, 4);
	
	spawnSwarm(&swarm1);
}

void step() {
	switch (GAMESTATE)
	{
		case GSTATE_MENU:
			if (keyPressed(KEY_UP)) menu.selindex--;
			if (keyPressed(KEY_DOWN)) menu.selindex++;
			if (keyPressed(KEY_FIRE)) {
				if (menu.selindex == 0) GAMESTATE = GSTATE_PLAY;
			};
			break;
		case GSTATE_PLAY:
			if (keyPressed(KEY_UP)) plr.y--;
			if (keyPressed(KEY_DOWN)) plr.y++;
			if (keyPressed(KEY_LEFT)) plr.x--;
			if (keyPressed(KEY_RIGHT)) plr.x++;
			if (keyPressed(KEY_FIRE) == 1) {
				for (uint8_t i; i < 4; i++) {
					if (!(bullets[i].flags & 1)) {
						bullets[i].flags ^= 1;
						bullets[i].x = plr.x;
						bullets[i].y = plr.y;
						break;
					};
				};
			};


			if (enemyCount > 0) {
				enemyUpdateTimer--;
				if (!enemyUpdateTimer) {
					enemyUpdateTimer = CEL_ENEMY_TICRATE;
		
					for (uint8_t i = 0; i < enemyCount; i++) {
						if (!(enemies[i].flags & 1)) continue;
						enemies[i].y++;
						enemies[i].bbox.x1 = enemies[i].x - 4;
						enemies[i].bbox.x2 = enemies[i].x + 4;
						enemies[i].bbox.y1 = enemies[i].y - 4;
						enemies[i].bbox.y2 = enemies[i].y + 4;
					};
				};
			};
			
			for (uint8_t i = 0; i < 4; i++) {
				if (bullets[i].flags & 1) {
					bullets[i].y -= 4;
					if (enemyCount > 0) {
						for (uint8_t e = 0; e < enemyCount; e++) {
							if (!(enemies[e].flags & 1)) continue;
							if (pointCollidesWithBox(bullets[i].x, bullets[i].y, &enemies[e].bbox)) {
								enemies[e].flags ^= 1;
								bullets[i].flags ^= 1;
							};
						};
					};
					if (bullets[i].y > 64) bullets[i].flags ^= 1;
				};
			};

			break;
	}
}
