// SAF frontend for Celestia. Defaults to using SDL2.
// Licensed under CC0

#define SAF_PROGRAM_NAME "Celestia"
#define SAF_PLATFORM_SDL2
#include "saf.h"

#include "game.h"
#include "sprites.h"

uint8_t keyPressed(uint8_t index) {
	switch (index)
	{
		case KEY_UP: return SAF_buttonPressed(SAF_BUTTON_UP); break;
		case KEY_DOWN: return SAF_buttonPressed(SAF_BUTTON_DOWN); break;
		case KEY_LEFT: return SAF_buttonPressed(SAF_BUTTON_LEFT); break;
		case KEY_RIGHT: return SAF_buttonPressed(SAF_BUTTON_RIGHT); break;
		case KEY_FIRE: return SAF_buttonPressed(SAF_BUTTON_A); break;
	};
};

void SAF_init() {
	start();
};

uint8_t sineBob = 128;

uint8_t SAF_loop() {
	step();
	SAF_clearScreen(SAF_COLOR_BLACK);
	switch (GAMESTATE)
	{
		case GSTATE_MENU:
			SAF_drawText("CELESTIA", 10, 16 + SAF_sin(sineBob) / 16, 255, 1);
			SAF_drawText("Press A to", 2, 48, 255, 1);
			SAF_drawText("begin", 20, 52, 255, 1);
			sineBob += 4;
			break;
		case GSTATE_PLAY:
			SAF_drawImage(plrIMG, plr.x - 4, plr.y - 4, 0, 0);
			for (uint8_t i = 0; i < enemyCount; i++) {
				SAF_drawRect(enemies[i].bbox.x1, enemies[i].bbox.y1, 8, 8, 255, 0);
				if (enemies[i].flags & 1)
					SAF_drawImage(enemy1IMG, enemies[i].x - 4, enemies[i].y - 4, 0, 0);
			};
			
			for (uint8_t i = 0; i < 4; i++) {
				if (bullets[i].flags & 1) SAF_drawPixel(bullets[i].x, bullets[i].y, 255);
			}
			break;
	}
	return 1; // how come it only runs when I do this? fml
};
