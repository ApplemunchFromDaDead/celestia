# Celestia (working title?)

*public domain free-as-in-freedom space shoot-em-up*

## description

Celestia is a shoot-em-up game inspired by the likes of Shoot-Em-Dolphi and old arcade games. Not much else is to be said; there's no lore, no extremely detailed mechanics, but there is a few things I'd like to mention of its qualities:

- Made with the suckless philosophy; no required file I/O, all assets are embedded into the executable, programmed in C99 with minimal fluff
- Modding is made easy with fair documentation (usually in source code) and scripts for converting to game data formats (e.g. for enemy swarm patterns)
- Runs primarily on drummyfish's SAF engine, allowing for easy portability
- Feels like good old Galaga

## license and "rights"
*Copied from drummyfish's Anarch README with minor modification*
